configArr=$(aws ecs list-tasks --cluster lab5-stack-lramirez --region us-east-2)
for config in $(echo "${configArr}" | jq -r '.[]' | sed -e 's/[\[]/''/g' | sed -e 's/[]]/''/g' | tr "," "\n" | tr '"' "\n" ) ; do
    aws ecs stop-task  --task "$config" --cluster lab5-stack-lramirez --region us-east-2
done
