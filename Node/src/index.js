var express = require("express"); //Llamada a la librería express
var bodyParser = require("body-parser");//Lectura de parámetros
var app = express(); //Instancia dentro de la aplicación
var redis = require('redis');


app.use(express.static('public')); //Carpetas de acceso público
app.use(bodyParser.json()); //Peticion con el formato application/json
app.use(bodyParser.urlencoded({extended: true}));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

var client = redis.createClient('6379', process.env.RedisHost);

app.get("/", function(req, res) {
	console.log(process.env.RedisHost);
	res.render("index");
});

app.post("/users", function(req, res){

	client.set(req.body.email, JSON.stringify(req.body));
	res.send("successful" );

});

app.post("/show", function(req, res){

	client.get(req.body.email, (err, value)=>{
		if (err) {
			res.send(err);
		}
		console.log(req.body.email+":" + value);
		res.send(value);
	});

});

app.listen(3000);
